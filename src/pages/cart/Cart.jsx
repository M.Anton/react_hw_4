import { useEffect, useRef, useState } from 'react';
import ProductCard from '../../components/productCard/ProductCard';
import Modal from '../../components/Modal';
import { useDispatch, useSelector } from 'react-redux';
import { closeDeleteModalAction, openDeleteModalAction } from '../../store/modalsReducer';

function Cart({updateAddedToCartProductsCount}){
    let [noAddedToCartProducts, setNoAddedToCartProducts] = useState(false)
    let [productsInCart, setProductsInCart] = useState([]);
    const addedToCartProductsId = useRef([]);
    const deleteFromCartProductsId = useRef([]);

    const isDeleteModalOpen = useSelector(state => state.modals.deleteModalState)
    const dispatch = useDispatch();

    const scrollY = useRef(0); 

    function deleteProductFromCart(){
        closeModal();
        let updatedAddedToCartProductsId = [...addedToCartProductsId.current].filter(id => id!==deleteFromCartProductsId.current);
        localStorage.setItem('addedToCartProducts', JSON.stringify(updatedAddedToCartProductsId));
        console.log('PRODUCT ID TO DELETE!!!', deleteFromCartProductsId.current);
        let updatedProductsInCart = productsInCart.filter(product => product.id !== deleteFromCartProductsId.current);
        setProductsInCart(updatedProductsInCart);
        updateAddedToCartProductsCount();
    }

    function openDeleteModal(event, id){
        scrollY.current = window.scrollY;
        dispatch(openDeleteModalAction())
        document.body.style.position = 'fixed';
        document.body.style.width = '100%';
        document.body.style.boxSizing = 'border-box';
        document.body.style.top = `-${scrollY.current}px`;
        document.body.style.paddingRight = '17px';
        
        deleteFromCartProductsId.current = id;
        console.log('addedToCartProductsId.current', addedToCartProductsId.current);
    }

    function closeModal(){
        dispatch(closeDeleteModalAction())
        document.body.style.position = '';
        document.body.style.top = ``;
        document.body.style.width = '';
        document.body.style.boxSizing = '';
        document.body.style.top = `-${scrollY.current}px`;
        window.scrollTo(0, scrollY.current);
        document.body.style.paddingRight = '';
    }

    useEffect(()=>{
        fetch('products.json')
        .then(res => res.json())
        .then(data => {
            setProductsInCart([...data.products].filter(product=>
                addedToCartProductsId.current.includes(product.id)
            ))
        })
        .catch(error => console.log('An error occured while fetching all products: ', error))
    }, [])

    useEffect(()=>{
        let addedToCartProducts = JSON.parse(localStorage.getItem('addedToCartProducts'));
        addedToCartProductsId.current = addedToCartProducts;
        if(addedToCartProducts.length === 0){
            setNoAddedToCartProducts(true)
        }
        console.log("addedToCartProductsId: ", addedToCartProductsId.current);
    }, [productsInCart])

    return (
        <div className="cardsWrapper">
            {/* При noAddedToCartProducts === true відображаємо надпис що немає товарів*/}
            {noAddedToCartProducts 
            ?   <h1 className='noProductsTitle'>There is no added to cart products yet</h1> 
            :   null 
            }
            {productsInCart.map(product => <ProductCard
                                                key={product.setNumber}
                                                id={product.id}
                                                name={product.name}
                                                price={product.price}
                                                description={product.description}
                                                color={product.color}
                                                imgUrl={product.imgUrl}
                                                deleteButton={true}
                                                openDeleteModal={openDeleteModal}
                                            />)}
            
            {isDeleteModalOpen && 
                <Modal
                backgroundColor='linear-gradient(to bottom, #D44638 33%, #E74C3D 24% 100%)'
                header='Do you want to delete this product from a cart?'
                closeButton='true'
                // closeButton='false'
                text='If you delete the product you can still add it to a cart from home page'
                actions={<><button className='buttonForDeleteModal' onClick={deleteProductFromCart}>Yes</button>
                <button className='buttonForDeleteModal' onClick={closeModal}>No</button></>}
                closeModal={closeModal}
            />
            }
            
        </div>
    )
}

export default Cart