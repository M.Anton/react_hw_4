import { fetchProductsAction } from "../store/productsReducer"

export function fetchProducts() {
    return function(dispatch){
        fetch('products.json')
        .then(res => res.json())
        .then(data => dispatch(fetchProductsAction(data.products)))
        .catch(error => console.log('An error occured while fetching all products: ', error))
    }
}